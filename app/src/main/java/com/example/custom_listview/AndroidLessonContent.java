package com.example.custom_listview;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class AndroidLessonContent extends AppCompatActivity {

    ListView lessonContentListView;
    String contentList[] = {"1.1: Android Studio and Hello World",
            "1.2 Part A: Your first interactive UI",
            "1.2 Part B: The layout editor",
            "1.3: Text and scrolling views",
            "1.4: Learn to help yourself"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_content);

        lessonContentListView = (ListView)findViewById(R.id.customListViewLessonContent);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview, R.id.textView, contentList);
        lessonContentListView.setAdapter(arrayAdapter);

        lessonContentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(AndroidLessonContent.this, LessonDetail.class);
                intent.putExtra("LESSON","ANDROID");
                startActivity(intent);
            }
        });

    }
}
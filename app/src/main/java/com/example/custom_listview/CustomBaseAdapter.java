package com.example.custom_listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomBaseAdapter extends BaseAdapter {
    Context context;
    String listCourseTitle[];
    String listCourseDescription[];
    int listCourseImage[];
    LayoutInflater inflater;

    public CustomBaseAdapter(Context ctx, String [] listCourseTitle, String [] listCourseDescription, int [] listCourseImage){
        this.context =ctx;
        this.listCourseTitle = listCourseTitle;
        this.listCourseDescription = listCourseDescription;
        this.listCourseImage = listCourseImage;
        this.inflater = LayoutInflater.from(ctx);
    }

    @Override
    public int getCount() {
        return listCourseTitle.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.activity_custom_list_view, null);
        TextView textviewTitle = (TextView) view.findViewById(R.id.textTitle);
        TextView textviewDescription = (TextView) view.findViewById(R.id.textDescription);
        ImageView imageViewLogo = (ImageView) view.findViewById(R.id.imageCourseLogo);
        textviewTitle.setText(listCourseTitle[i]);
        textviewDescription.setText(listCourseDescription[i]);
        imageViewLogo.setImageResource(listCourseImage[i]);
        return view;
    }
}

package com.example.custom_listview;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

public class JavaLessonContent extends AppCompatActivity {

    ListView lessonContentListView;
    String contentList[] = {"Home",
            "Overview",
            "Environment Setup",
            "Basic Syntax",
            "Object & Class"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_content);

        lessonContentListView = (ListView)findViewById(R.id.customListViewLessonContent);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.activity_listview, R.id.textView, contentList);
        lessonContentListView.setAdapter(arrayAdapter);

        lessonContentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(JavaLessonContent.this, LessonDetail.class);
                intent.putExtra("LESSON","JAVA");
                startActivity(intent);
            }
        });
    }
}
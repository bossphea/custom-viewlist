package com.example.custom_listview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
public class LessonDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_detail);

        WebView webView = (WebView) findViewById(R.id.lessonDetail);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        //webView.loadUrl("https://www.journaldev.com");
        webView.loadData(this.generateHtml(intent.getStringExtra("LESSON")), "text/html", "UTF-8");
    }

    private String generateHtml(String lessonTitle){
        if (lessonTitle.equals("ANDROID")){
            return "ANDROID";
        } else if (lessonTitle.equals("JAVA")){
            return "JAVA";
        } else if (lessonTitle.equals("PHP")){
            return "PHP";
        } else if (lessonTitle.equals("C++")){
            return "C++";
        } else if (lessonTitle.equals("C")){
            return "C";
        } else if (lessonTitle.equals("C#")){
            return "C#";
        } else {
            return "HELLO WORLD";
        }
    }
}
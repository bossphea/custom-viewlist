package com.example.custom_listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    String courseTitleList[] = {"Android", "Java", "PHP", "C++", "C", "C#"};
    String courseDescriptionList[] = {"Android is the most popular mobile Os.",
            "Java Language is the most popular both mobile and desktop",
            "PHP is the most popular for web development",
            "c++ is the most popular for game development",
            "C programming is the most popular for writing system embedded.",
            "C sharp is the most popular for desktop app"};
    int courseImageList[] = {R.drawable.android,
            R.drawable.java,
            R.drawable.php,
            R.drawable.cplusplus,
            R.drawable.c,
            R.drawable.csharp};

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.customListView);

        CustomBaseAdapter customBaseAdapter = new CustomBaseAdapter(getApplicationContext(), courseTitleList, courseDescriptionList, courseImageList);

        listView.setAdapter(customBaseAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0: startActivity(new Intent(MainActivity.this, AndroidLessonContent.class));
                        break;
                    case 1: startActivity(new Intent(MainActivity.this, JavaLessonContent.class));
                        break;
                    case 2: startActivity(new Intent(MainActivity.this, PhpLessonContent.class));
                        break;
                    case 3: startActivity(new Intent(MainActivity.this, CPlusPlusLessonContent.class));
                        break;
                    case 4: startActivity(new Intent(MainActivity.this, CLessonContent.class));
                        break;
                    case 5: startActivity(new Intent(MainActivity.this, CSharpLessonContent.class));
                        break;
                }
            }
        });
    }
}